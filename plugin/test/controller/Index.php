<?php
/**
 * @copyright   Copyright (c) 2017-2030  https://www.sapixx.com All rights reserved.
 * @license Licensed (https://www.gnu.org/licenses/gpl-3.0.txt).
 * @link https://www.sapixx.com
 * 扩展租户端
 */
namespace plugin\test\controller;
use base\TenantController;

class Index extends TenantController
{

    /**
     * 首页
     */
    public function index()
    {
        $this->bread([['name' =>'扩展首页']]);
        return view();
    }
    
    /**
     * 首页
     */
    public function act()
    {
       return IS_JSON?exitjson(200,'通过应用操作实例(JSON模板)'):view();
    }
}