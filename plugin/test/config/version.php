<?php
return [
    //应用名称
    'name'               => '扩展实例',
    //应用描述(40个字以内)
    'about'              => "智者|Sapi++是SaaS应用开发框架,基于TP6开发,开发实例是帮助你快速入门开发",
    //官网网站(可留空)
    'uri'                => 'https://www.sapixx.com',
    //版本号
    'var'                => '1.0.0',
    //是否有管理端
    'is_admin'           => true,
    //入口是否在siderBar,开启后tenant.php菜单(按二维数组设置)
    'siderbar'          => [
        'name' => '演示',
        'icon' => 'terminal-plus', 
    ],
    //开启关联扩展,开启后必须先开通关联扩展才能开通当前扩展
    'together'         => [],   //['plugin1','plugin2']
];