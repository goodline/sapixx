<?php
/**
 * 租户端管理菜单 
 * icon:https://icons.bootcss.com/
 * 在version,开启siderBar,按二维数组,设置菜单
 * ###########################开启siderBar,
 * return [
 *     [
 *         'name' => '扩展实例', //组名称
 *         'icon' => 'house',   //组ICON
 *         'menu' => [
 *                 ['name' =>'扩展开发','url'=> (string)plugurl('test/index/index')],
 *          ]
 *     ],
 *     [
 *         'name' => '扩展实例', //组名称
 *         'icon' => 'house',   //组ICON
 *         'menu' => [
 *                 ['name' =>'扩展开发','url'=> (string)plugurl('test/index/index')],
 *          ]
 *     ]
 * ];
 * ###########################关闭siderBar,
 * return [
 *   'name' => '扩展实例', //组名称
 *   'icon' => 'house',   //组ICON
 *   'menu' => [
 *       ['name' =>'扩展开发','url'=> (string)plugurl('test/index/index')],
 *    ]
 * ];
 */
return [
    [
    'name' => '扩展实例', //组名称
    'icon' => 'house',   //组ICON
    'menu' => [
            ['name' =>'扩展开发','url'=> (string)plugurl('test/index/index')],
        ]
    ]
];