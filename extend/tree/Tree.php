<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 无限分类
 */

namespace tree;

class Tree
{

	/**
	 * 原始数据
	 * @var array
	 */
	private $rawList = [];

	/**
	 * 格式化数据
	 * @var array
	 */
	private $formatList = [];

	/**
	 * 分类样式
	 * @var array
	 */
	private $icon = ['│ ', '├ ', '└ '];

	/**
	 * 映射字段
	 * @var array
	 */
	private $field = [];

	/**
	 * 构建函数
	 * @param array $field 字段映射
	 */
	public function __construct($field = [])
	{
		$this->field['id']        = isset($field['0']) ? $field['0'] : 'id';
		$this->field['pid']       = isset($field['1']) ? $field['1'] : 'pid';
		$this->field['title']     = isset($field['2']) ? $field['2'] : 'title';
		$this->field['fulltitle'] = isset($field['3']) ? $field['3'] : 'fulltitle';
		$this->field['children'] = isset($field['4']) ? $field['4'] : 'children';
	}

	/**
	 * 获取同级分类
	 * @param  integer $pid  上级分类
	 * @param  array  $data  分类数组
	 * @return array
	 */
	public function getChild($pid, $data = [])
	{
		$childs = [];
		if (empty($data)) {
			$data = $this->rawList;
		}
		foreach ($data as $Category) {
			if ($Category[$this->field['pid']] == $pid) {
				$childs[] = $Category;
			}
		}
		return $childs;
	}

	/**
	 * 获取树形分类
	 * @param  array   $data 分类数组
	 * @param  integer $id   起始上级分类
	 * @return array
	 */
	public function getTree($data, $id = 0)
	{
		if (empty($data)) {
			return false;
		}
		$this->rawList    = [];
		$this->formatList = [];
		$this->rawList    = $data;
		$this->_searchList($id);
		return $this->formatList;
	}

	/**
	 * 将一个平面的二维数组按照指定的字段转换为树状结构
	 *
	 * 用法：
	 * @code php
	 * $rows = [
	 *     ['id' => 1, 'value' => '1-1', 'parent' => 0],
	 *     ['id' => 2, 'value' => '2-1', 'parent' => 0],
	 *     ['id' => 3, 'value' => '3-1', 'parent' => 0],
	 *     ['id' => 7, 'value' => '2-1-1', 'parent' => 2],
	 *     ['id' => 8, 'value' => '2-1-2', 'parent' => 2],
	 *     ['id' => 9, 'value' => '3-1-1', 'parent' => 3],
	 *     ['id' => 10, 'value' => '3-1-1-1', 'parent' => 9],
	 * ];
	 *
	 * $tree = arraytotree($rows);
	 * 输出结果为：
	 * [
	 *   ['id' => 1, ..., 'nodes' => []],
	 *   ['id' => 2, ..., 'nodes' => [
	 *        [..., 'parent' => 2, 'nodes' => []),
	 *        [..., 'parent' => 2, 'nodes' => []),
	 *   ),
	 *   ['id' => 3, ..., 'nodes' => [
	 *        ['id' => 9, ..., 'parent' => 3, 'nodes' => [
	 *             [..., , 'parent' => 9, 'nodes' => [],
	 *        ],
	 *   ],
	 * ]
	 * @endcode
	 * 如果要获得任意节点为根的子树，可以使用 $refs 参数：
	 * @code php
	 * $refs = null;
	 * $tree = arraytotree($rows, $refs);
	 * 输出 id 为 3 的节点及其所有子节点
	 * $id = 3;
	 * dump($refs[$id]);
	 * @endcode
	 * @param $arr 数据源
	 * @param null $refs 是否在返回结果中包含节点引用
	 * @return array
	 */
	public function toTree($arr, & $refs = null)
	{
		$refs = [];
		foreach ($arr as $offset => $row) {
			$arr[$offset][$this->field['children']] = [];
			$refs[$row[$this->field['id']]] =& $arr[$offset];
		}
		$tree = [];
		foreach ($arr as $offset => $row) {
			$parentId = $row[$this->field['pid']];
			if ($parentId) {
				if (!isset($refs[$parentId])) {
					$tree[] =& $arr[$offset];
					continue;
				}
				$parent =& $refs[$parentId];
				$parent[$this->field['children']][] =& $arr[$offset];
			} else {
				$tree[] =& $arr[$offset];
			}
		}
		return $tree;
	}

	/**
	 * 获取目录数
	 * @param  array   $data 分类数组
	 * @param  integer $id   起始上级分类
	 * @return array
	 */
	public function getGroup($data, $id = 0)
	{
		if (empty($data)) {
			return false;
		}
		$this->rawList    = [];
		$this->formatList = [];
		$this->rawList    = $data;
		$this->formatList = $this->_searchGroup($id);
		return $this->formatList;
	}

	/**
	 * 获取分类路径
	 * @param  array  $data 分类数组
	 * @param  integer $id  当前分类
	 * @return array
	 */
	public function getPath($data, $id)
	{
		$this->rawList = $data;
		while (1) {
			$id = $this->_getPid($id);
			if ($id == 0) {
				break;
			}
		}
		return array_reverse($this->formatList);
	}

	/**
	 * 递归分类
	 * @param  integer $id    上级分类ID
	 * @param  string  $space 空格
	 * @return void
	 */
	private function _searchGroup($id = 0)
	{
		//下级分类的数组
		$childs = $this->getChild($id);
		if (!($n = count($childs))) {
			return;
		}
		foreach ($childs as $key => $value) {
			$children = $this->_searchGroup($value['id']);
			if (!empty($children)) {
				$childs[$key][$this->field['fulltitle']] =  $children;
			}
		}
		return $childs;
	}

	/**
	 * 递归分类
	 * @param  integer $id    上级分类ID
	 * @param  string  $space 空格
	 * @return void
	 */
	private function _searchList($id = 0, $space = "", $level = 1)
	{
		//下级分类的数组
		$childs = $this->getChild($id);
		//如果没下级分类，结束递归
		if (!($n = count($childs))) {
			return;
		}
		$cnt = 1;
		//循环所有的下级分类
		for ($i = 0; $i < $n; $i++) {
			$pre = "";
			$pad = "";
			if ($n == $cnt) {
				$pre = $this->icon[2];
			} else {
				$pre = $this->icon[1];
				$pad = $space ? $this->icon[0] : "";
			}
			$childs[$i][$this->field['fulltitle']] = ($space ? $space . $pre : "") . $childs[$i][$this->field['title']];
			$childs[$i]['level'] = $level;
			$this->formatList[] = $childs[$i];
			//递归下一级分类
			$this->_searchList($childs[$i][$this->field['id']], $space . $pad . "　", $level + 1);
			$cnt++;
		}
	}

	/**
	 * 获取PID
	 * @param  integer $id 当前ID
	 * @return integer
	 */
	private function _getPid($id)
	{
		foreach ($this->rawList as $key => $value) {
			if ($this->rawList[$key][$this->field['id']] === $id) {
				$this->formatList[] = $this->rawList[$key];
				return $this->rawList[$key][$this->field['pid']];
			}
		}
		return 0;
	}
}
