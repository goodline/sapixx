<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 基础模型
 */

namespace base;

use think\Request;

class BaseModel extends \think\Model
{
    /**
     * 自定义排序公共字段,默认排序字段sort
     * @param  integer $id  条件ID
     * @param  integer $sort  排序字段
     * @param  string  $field
     * @return json
     */
    public function sort(int $id, int $sort = 0, $field = 'sort')
    {
        try {
            $result = $this->apps()->field($field)->find($id);
            if(!$result) {
                return enjson(10005);
            }
            $result->$field = $sort;
            $result->save();
            return enjson();
        } catch (\Exception $e) {
            return enjson(0, $e->getMessage());
        }
    }

    /**
     * 字段状态修改器
     * @param  integer $id    条件ID
     * @param  string  $field 状态字段名
     * @return json
     */
    public function state(int $id, string $field = 'state')
    {
        try {
            $result = $this->apps()->field($field)->find($id);
            if(!$result) {
                return enjson(10005);
            }
            $result->$field = $result->$field ? 0 : 1;
            $result->save();
            return enjson();
        } catch (\Exception $e) {
            return enjson(0, $e->getMessage());
        }
    }

    /**
     * 小程序查询条件
     * @param integer $apps_id
     * @return object
     */
    public function scopeApps($query, int $apps_id = 0)
    {
        return $this->invoke(fn (Request $request) => $query->where('apps_id', $apps_id ?: ($request->apps->id ?? 0)));
    }

    /**
     * 租户查询条件
     * @param integer $apps_id
     * @return object
     */
    public function scopeTenant($query, int $tenant_id = 0)
    {
        return $this->invoke(fn (Request $request) => $query->where('tenant_id', $tenant_id ?: ($request->tenant->id ?? 0)));
    }

    /**
     * 注册用户查询条件(请确保应用表中有UID字段再使用)
     * @param integer $uid
     * @return object
     */
    public function scopeUid($query, int $uid = 0)
    {
        return $this->invoke(fn (Request $request) => $query->where('uid', $uid ?: ($request->user->id ?? 0)));
    }

    /**
     * 插件查询条件(请确保应用表中有plugin_id字段再使用)
     * @param integer $uid
     * @return object
     */
    public function scopePlugin($query, int $plugin_id)
    {
        return $query->where('plugin_id', $plugin_id);
    }

    /**
     * 根据经纬度查询一个圆周内的距离
     * 使用本方法表中必须有经longitude,纬latitude两个字段
     * @param float   $lng 经
     * @param float   $lat 纬
     * @param integer $km  范围距离
     * @return object
     */
    public function scopeLbs($query, $lng = 0, $lat = 0, $km = 2)
    {
        return $query->whereRaw('latitude > '.$lat.'-'.$km.' and latitude < '.$lat.'+'.$km.' and longitude > '.$lng.'-'.$km.' and longitude < '.$lng.'+'.$km)->orderRaw('ACOS(SIN(('.$lat.' * 3.1415) / 180 ) *SIN((latitude * 3.1415) / 180 ) +COS(('.$lat.' * 3.1415) / 180 ) * COS((latitude * 3.1415) / 180 ) *COS(('.$lng.'* 3.1415) / 180 - (longitude * 3.1415) / 180 ) ) * 6380 asc');
    }

    /**
     * 一对一关联
     * 管理查询前台登录用户信息(请确保应用表中有UID字段再使用)
     * @return object
     */
    public function user()
    {
        return $this->hasOne('base\model\SystemUser', 'id', 'uid')->field(['id','apps_id','invite_code','phone','nickname','face'])->append(['as_phone']);
    }
}
