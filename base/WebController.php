<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * PC和H5基础控制器
 */

namespace base;

class WebController extends BaseController
{
    //定义默认中间件
    protected $middleware  = ['base\middleware\Web'];

    //强制登录验证
    protected $acl         = [];

    //免于登录控制
    protected $aclOff      = [];

    /**
     * 初始化
     */
    protected function initialize()
    {
        if(!empty($this->acl) || !empty($this->aclOff)) {
            if(empty($this->acl)) {
                $this->middleware['base\middleware\ApiAcl::class'] = ['except' => $this->aclOff];
            } else {
                $this->middleware['base\middleware\ApiAcl::class'] = ['only' => $this->acl];
            }
        }
    }
}
