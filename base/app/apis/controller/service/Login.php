<?php
/**
 * @copyright   Copyright (c) 2017-2030  https://www.sapixx.com All rights reserved.
 * @license Licensed (https://www.gnu.org/licenses/gpl-3.0.txt).
 * @link https://www.sapixx.com
 * 微信认证服务
 */

namespace base\app\apis\controller\service;

use base\ApiController;
use base\model\SystemUser;
use base\model\SystemUserRuid;

class Login extends ApiController
{
    //请求方式
    protected $method = [
        'weapp' => 'post',
    ];

    /**
    * 小程序登录接口
    * @return void
    */
    public function weapp()
    {
        if(!$this->request->client) {
            return enjson(11005);
        }
        $param = [
            'code'   => $this->request->param('code/s'),
            'invite' => $this->request->param('invite/s', ''),
        ];
        $this->validate($param, 'Login.weapp');
        $data = [
            'appid'      => $this->request->client->appid,
            'js_code'    => $param['code'],
            'grant_type' => 'authorization_code',
        ];
        $is_open_wechat = $this->request->app->config['is_open_wechat']??false;
        if($is_open_wechat) {
            $app = app('wechat')->openPlatform();
            $data['component_appid']        = $app->getAccount()->getAppId();
            $data['component_access_token'] = $app->getComponentAccessToken()->getToken();
            $response = $app->getClient()->get('sns/component/jscode2session', $data);
            if ($response->isFailed()) {
                return enjson(10001, $response['errmsg']);
            }
        } else {
            $data['secret'] = $this->request->client->secret;
            $response = app('wechat')->data($data)->get('sns/jscode2session');
        }
        //自动判断是登录还是注册
        $rel = SystemUser::addWechatUser([
            'client_id' => $this->request->client->id,
            'apps_id'   => $this->request->apps->id,
            'openid'    => $response['openid'],
            'unionid'   => $response['unionid']??'',
            'invite'    => $param['invite']
        ]);
        if(empty($rel)) {
            return enjson(11104, '账号认证失败或已被锁定');
        }
        //获取(注册/登录)数据
        $result = SystemUserRuid::addWechatUserRuid([
            'uid'       => $rel->id,
            'client_id' => $this->request->client->id,
            'apps_id'   => $this->request->apps->id,
            'login_id'  => $response['openid'],
            'secret'    => $response['session_key']
        ]);
        if(empty($result)) {
            return enjson(403, '账号认证失败');
        }
        return enjson(200, [
            'uid'               => $rel->id,
            'token'             => app('jwt')->enJwt($rel->id),
            'is_getUserProfile' => ($rel->getData('nickname') && $rel->getData('face')) ? true : false, //兼容微信新接口,判断是否设置了用户头像或昵称,用于客户端判断
            'ucode'             => $rel->invite_code,
            'session_id'        => app('session')->getId()
        ]);
    }

    /**
     * 公众号接口
     * @return void
     */
    public function official()
    {
        //开始获取登录信息
        if(!$this->request->client) {
            $this->error('应用未找到或禁用');
        }
        $param = [
            'get'      => $this->request->param('get/s'),
            'code'     => $this->request->param('code/s'),
            'state'    => $this->request->param('state/s'),
            'invite'   => $this->request->param('invite/s', ''),
            'callback' => $this->request->param('callback/s'),
        ];
        $this->validate($param, 'Login.wemp');
        //获取账号授权信息
        try {
            $response = app('wechat')->new()->getOauth()->scopes(['snsapi_userinfo'])->userFromCode($param['code']);
            $data = $response->getRaw();
            if(isset($data['errcode'])) {
                abort(403, $data['errmsg']);
            }
            $openid = $response->getId();
            if(empty($openid)) {
                abort(403, '微信账号授权失败');
            }
            //获取unionid
            if(empty($data['unionid'])) {
                $union = app('wechat')->client('wechatmp')->data(['openid' => $openid])->get('cgi-bin/user/info');
                $unionid = $union['unionid']??'';
            } else {
                $unionid = $data['unionid'];
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        //判断是登录还是注册
        $rel = SystemUser::addWechatUser([
            'apps_id'   => $this->request->apps->id,
            'client_id' => $this->request->client->id,
            'openid'    => $openid,
            'unionid'   => $unionid,
            'nickname'  => $response->getNickname(),
            'face'      => $response->getAvatar(),
            'invite'    => $param['invite'],
        ]);
        if(empty($rel)) {
            $this->error('账号认证失败或已被锁定');
        }
        //获取(注册/登录)数据
        $result = SystemUserRuid::addWechatUserRuid([
            'uid'       => $rel->id,
            'client_id' => $this->request->client->id,
            'apps_id'   => $this->request->apps->id,
            'login_id'  => $response->getId(),
            'secret'    => $param['state']
        ]);
        if(empty($result)) {
            $this->error('账号认证失败');
        }
        $this->app->user->setLogin($rel->id);
        if(!empty($param['callback'])) {
            return redirect(urldecode($param['callback']));
        }
        return redirect((string)url('web/'.$param['get'].'/index', ['token' => app('jwt')->enJwt($rel->id)]));
    }
}
