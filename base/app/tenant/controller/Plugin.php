<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 扩展商店
 */

namespace base\app\tenant\controller;

use base\model\SystemPlugin;
use base\model\SystemPlugins;
use base\model\SystemTenant;
use base\model\SystemTenantBill;
use base\model\SystemTenantGroup;
use util\Ary;

class Plugin extends Common
{
    protected $middlewares = ['base\app\tenant\middleware\Manage'];

    /**
     * 扩展商店
     * @access public
     */
    public function index()
    {
        $view['list']     = SystemPlugins::where(['apps_id' => $this->request->apps->id,'tenant_id' => $this->request->tenant->id])->order('id desc')->paginate($this->pages);
        $view['is_group'] = SystemTenantGroup::apps()->count();
        $this->bread([['name' =>'扩展中心'],['name' =>'我的扩展']]);
        return view()->assign($view);
    }

    /**
     * 扩展商店
     * @access public
     */
    public function store()
    {
        $plugin_id    = SystemPlugins::where(['apps_id' => $this->request->apps->id,'tenant_id' => $this->request->tenant->id])->column('plugin_id');
        $view['list'] = SystemPlugin::where(['is_lock' => 0])->whereNotIn('id', $plugin_id)->whereFindInSet('app_ids', $this->request->app->id)->order('sort desc,id desc')->paginate($this->pages);
        $this->bread([['name' =>'扩展中心'],['name' =>'扩展商店']]);
        return view()->assign($view);
    }

    /**
     * 权限组
     * @access public
     */
    public function group(int $id)
    {
        $info = SystemPlugins::where(['id' => $id])->apps()->find();
        if(!$info) {
            $this->error('未找到扩展');
        }
        if (IS_POST) {
            $group = $this->request->param('group/a');
            if (empty($group)) {
                return enjson(403, '最少要选择一个组');
            }
            $info->group_ids = Ary::array_int($group);
            $info->save();
            return enjson(200);
        } else {
            $view['list'] = SystemTenantGroup::apps()->select();
            $view['info'] = $info;
            return view()->assign($view);
        }
    }

    /**
     * 购买扩展
     * @param integer $id
     * @return void
     */
    public function onbuy(int $win = 0)
    {
        if (request()->isPost()) {
            $data = [
                'plugin_id'    => $this->request->param('plugin_id/d'),
                'safepassword' => $this->request->param('safepassword/s'),
                'agree'        => $this->request->param('agree/d', 0)
            ];
            $this->validate($data, 'Apps.plugin');
            //判断安全密码是否正确
            if(!password_verify(md5($data['safepassword']), $this->request->tenant->safe_password)) {
                return enjson(403, '你输入的安全密码不正确');
            }
            //读取应用
            $plugin  = SystemPlugin::where(['id' => $data['plugin_id']])->find();
            if (empty($plugin)) {
                return enjson(403, '未找到要开通的扩展');
            }
            //判断关联扩展是否开通
            $together = $plugin->config['together']??[];
            if(!empty($together)) {
                $plugins = SystemPlugin::where(['is_lock' => 0,'appname' => $together])->order('sort desc,id desc')->select();
                $appname = array_column($plugins->toArray(), 'appname');
                if(count($together) != count($appname)) {
                    return enjson(403, '关联的扩展未找到');
                }
                foreach ($appname as $value) {
                    if(!is_plugin($value, true)) {
                        return enjson(403, '请先安装关联扩展,再启用此扩展');
                    };
                }
            }
            //判断关联插件是否开通
            if(SystemPlugins::where(['apps_id' => $this->request->apps->id,'plugin_id' => $plugin->id,'tenant_id' => $this->request->tenant->id])->count()) {
                return enjson(403, $this->request->apps->title.'应用下此扩展已启用过');
            }
            //判断帐号额度,如果价格<=0,就不在查询数据库
            if ($plugin->price > 0) {
                if (!SystemTenant::moneyDec($this->request->tenant->id, $plugin->price)) {
                    return enjson(403, '余额不足,请充值。');
                }
                SystemTenantBill::create([
                    'tenant_id' => $this->request->tenant->id,
                    'state'     => 1,
                    'money'     => -$plugin->price,
                    'message'   => '应用「'.$this->request->apps->title.'」启用「'.$plugin->title.'」扩展',
                    'order_sn'  => uuid()
                ]);
            }
            //创建开通扩展
            SystemPlugins::create([
                'plugin_id' => $plugin->id,
                'apps_id'   => $this->request->apps->id,
                'tenant_id' => $this->request->tenant->id
            ]);
            return $win ? enjson(202) : enjson(200, ['url' => (string)url('plugin/index')]);
        } else {
            $id   =  $this->request->param('id/d', 0);
            $info = SystemPlugin::where(['id' => $id])->find();
            if(!$info) {
                $this->error('未找到扩展');
            }
            $plugins = SystemPlugins::where(['apps_id' => $this->request->apps->id,'plugin_id' => $id])->find();
            if($plugins) {
                $this->error('此扩展不需要重复开启');
            }
            //读取关联插件
            $view['plugin'] = [];
            $together = $info->config['together']??[];
            if(!empty($together)) {
                $plugin = SystemPlugin::where(['is_lock' => 0,'appname' => $together])->order('sort desc,id desc')->select();
                $appname = array_column($plugin->toArray(), 'appname');
                if(count($together) != count($appname)) {
                    $this->error('关联的扩展功能未找到,已禁止启用.');
                }
                $view['plugin'] = $plugin;
            }
            $view['info'] = $info;
            $view['win']  = $win;
            if(!$win) {
                $this->bread([['name' =>'扩展中心'],['name' =>'扩展启用']]);
            }
            return view()->assign($view);
        }
    }
}
