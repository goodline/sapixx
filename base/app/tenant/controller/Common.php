<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 租户调用模块
 */
namespace base\app\tenant\controller;
use base\TenantController;

class Common extends TenantController{
    
    /**
     * 中台转入租户管理
     */
    public function index(){
        return redirect((string)url('index/index'),302);
    }

    /**
     * 文件资源管理器
     */
    public function storage(){
        return view();
    }
}