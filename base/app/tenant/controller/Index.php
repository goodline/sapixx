<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 租户端
 */
namespace base\app\tenant\controller;
use base\model\SystemTenant;
use base\model\SystemTenantApps;
use base\model\SystemApps;
use base\model\SystemTenantGroup;
use base\model\SystemPlugin;
use base\model\SystemPlugins;
use think\facade\Config;
use think\facade\Cookie;
use util\Ary;

class Index extends Common
{

    /**
     * 租户首页
     */
    public function index()
    {
        $view['is_siderbar'] = false;
        $view['is_full']     = false;
        if($this->request->apps){
            $view['is_full'] = empty(app('configs')->tenant($this->request->app->appname))?true:false;
            if(empty(app('configs')->siderbar($this->request->app->appname))){
                $plugin = SystemPlugin::whereFindInSet('app_ids',$this->request->app->id)->field('id,title,appname')->select();
                if(!$plugin->isEmpty()){
                    $condition['apps_id'] = $this->request->apps->id;
                    $condition['is_lock'] = 0;
                    if(!$this->request->tenant->is_manage && $this->request->tenant->group_id){
                        $plugin_ida = SystemPlugins::where($condition)->whereFindInSet('group_ids',$this->request->tenant->group_id)->count();
                    }else{
                        $plugin_ida = SystemPlugins::where($condition)->count();
                    }
                    if($plugin_ida){
                        $view['is_siderbar'] = true;
                    }
                }
            }else{
                $view['is_siderbar'] = true;
            }
        }
        return view()->assign($view);
    }

    /**
     * 应用控制台跳转
     */
    public function welcome()
    {
        if($this->request->apps){
            return redirect((string)url($this->request->app->appname.'/tenant/index'));
        }else{
            return redirect((string)url('store/index'));
        }
    }

   /**
     * 管理菜单
     * @return json
     */
    public function menu($menu = 'tenant')
    {
        try {
            if($this->request->apps){
                $plugin_name = '';
                $is_siderbar = strpos($menu,'plugin_') === 0?true:false;
                if($is_siderbar){
                    $plugin_name = substr($menu,7)??''; 
                    $menu        = 'tenant_plugin';
                }else{
                    $menu = (empty($menu) || $menu == 'tenant')?'tenant':'tenant_'.$menu; 
                }
                $version = $this->app->configs->version($this->request->app->appname);
                if(empty($version['is_setting']) && $this->request->tenant->is_manage){
                    if($menu == 'tenant_plugin'){ //扩展
                        $menus = array_merge($this->pluginMneu($plugin_name),$plugin_name?[]:config('tenant_plugin'));
                    }else{
                        $menus = $this->app->configs->tenant($this->request->app->appname,$menu);
                        if($menu == 'tenant' && !$this->request->tenant->lock_config){ //关于应用
                            $isPlugin = SystemPlugin::whereFindInSet('app_ids',$this->request->app->id)->count();
                            if($isPlugin){
                                $about = config('tenant');
                                array_unshift($about[0]['menu'],['name' =>'我的扩展','url'=> (string)url('plugin/index')]);
                                $menus = array_merge($menus,$about);
                            }else{
                                $menus = array_merge($menus,config('tenant'));
                            }
                        }
                    }
                }else{
                    if($menu == 'tenant_plugin'){ //扩展
                        $menus = $this->pluginMneu($plugin_name);
                    }else{
                        $menus = $this->app->configs->tenant($this->request->app->appname,$menu);   //全部菜单
                        if($this->request->tenant->group_id){
                            $info = SystemTenantGroup::where(['id' => $this->request->tenant->group_id])->apps()->field('menu')->find(); //读取数据库中权限组权限
                            $access_menu = empty($info)?[]:array_flip($info->menu);
                            //仅显示有选择的菜单
                            $data = [];
                            foreach ($menus as $key => $value) {
                                $data[$key]['name'] = $value['name'];
                                if(!empty($value['menu'])){
                                    foreach ($value['menu'] as $k => $val) {
                                        if(isset($val['url']) && isset($access_menu[md5($val['url'])])){
                                            $data[$key]['menu'][$k]['name'] = $val['name'];
                                            $data[$key]['menu'][$k]['url']  = $val['url'];
                                        }
                                    }
                                }
                                if(empty($data[$key]['menu'])){
                                    unset($data[$key]);
                                }
                            }
                            $menus = Ary::reform_keys($data);
                        }
                    }
                }
            }else{
                $menus = config('other');
            }
            return enjson(204,$menus);
        } catch (\Exception $e){
            return enjson(204);
        }
    }

    /**
     * 读取插件菜单
     * @param boolean $plugin 插件名称
     * @return void
     */
    protected function pluginMneu($plugin = ''){
        $plugin_menu = [];
        if(empty($plugin)){
            if(!$this->request->tenant->is_manage && $this->request->tenant->group_id){
                $plugin_id = SystemPlugins::where(['apps_id' => $this->request->apps->id,'is_lock' => 0])->whereFindInSet('group_ids',$this->request->tenant->group_id)->column('plugin_id');
            }else{
                $plugin_id = SystemPlugins::where(['apps_id' => $this->request->apps->id,'is_lock' => 0])->column('plugin_id');
            }
            if(!empty($plugin_id)){
                $plugin = SystemPlugin::whereIn('id',$plugin_id)->field('id,title,appname')->append(['config'])->select()->toArray();
                foreach ($plugin as $value) {
                    if(empty($value['config']['siderbar'])){
                        $plugin_menu[] =  $this->app->configs->plugin($value['appname'],'tenant');
                    }
                }
            }
        }else{
            $plugin = SystemPlugin::where(['appname' => $plugin])->field('id,title,appname')->append(['config'])->find();
            if(!$plugin){
                return $plugin_menu;
            }
            $plugins = SystemPlugins::where(['plugin_id' => $plugin->id,'apps_id' => $this->request->apps->id,'is_lock' => 0])->field('group_ids')->find();
            if(!$plugins){
                return $plugin_menu;
            }
            if(!$this->request->tenant->is_manage && $this->request->tenant->group_id && !in_array($this->request->tenant->group_id,$plugins->group_ids)){
                return $plugin_menu;
            }
            $plugin_menu = $this->app->configs->plugin($plugin->appname,'tenant');
        }
        return $plugin_menu;
    }

   /**
     * siderBar
     * @return json
     */
    public function siderbar()
    {
        try {
            $siderbar = [];
            if(!$this->request->apps){
                return enjson(204);
            }
            $siderbar = $this->app->configs->siderbar($this->request->app->appname);
            if(!$this->request->tenant->is_manage && $this->request->tenant->group_id){
                $info = SystemTenantGroup::where(['id' => $this->request->tenant->group_id])->apps()->find();
                $access_menu = $info?array_flip($info->menu):[];
                foreach ($siderbar as $key => $var) {
                    $siderbar[$key]['menu'] = 0;
                    $menu = app('configs')->tenant($this->request->app->appname,$var['anchor']=='tenant'?'tenant':'tenant_'.$var['anchor']);
                    foreach ($menu as $value) {
                        if(isset($value['menu'])){
                            foreach ($value['menu'] as $val) {
                                if(isset($val['url']) && isset($access_menu[md5($val['url'])])){
                                    ++$siderbar[$key]['menu'];
                                }
                            }
                        }
                    }
                    if($siderbar[$key]['menu'] == 0){
                        unset($siderbar[$key]);
                    }
                }
                $siderbar = Ary::reform_keys($siderbar);
            }
            //插件
            $plugin = SystemPlugin::whereFindInSet('app_ids',$this->request->app->id)->field('id,title,appname')->append(['config'])->select()->toArray();
            if(empty($plugin)){
                return enjson(204,$siderbar);
            }
            $plugin   = array_combine(array_column($plugin,'id'),$plugin); 
            if(!$this->request->tenant->is_manage && $this->request->tenant->group_id){
                $plugin_ida = SystemPlugins::where(['apps_id' => $this->request->apps->id,'is_lock' => 0])->whereFindInSet('group_ids',$this->request->tenant->group_id)->column('plugin_id');
            }else{
                $plugin_ida = SystemPlugins::where(['apps_id' => $this->request->apps->id,'is_lock' => 0])->column('plugin_id');
            }
            //判断插件菜单是不是显示在SiderBar
            $min_plugin   = 0;
            $plugin_sider = [];
            foreach ($plugin_ida as $value) {
                if(isset($plugin[$value])){
                    if(empty($plugin[$value]['config']['siderbar'])){
                        $min_plugin++;
                    }else{
                        $plugin_sider[] = [
                            'name'   => $plugin[$value]['config']['siderbar']['name']??$value['title'],
                            'icon'   => $plugin[$value]['config']['siderbar']['icon']??'terminal-plus',
                            'anchor' => 'plugin_'.$plugin[$value]['appname']
                        ];
                    }
                }
            }
            $plugin_home = [['name' =>'首页','icon' =>'house','anchor' =>'tenant']];
            $plugin_base = [['name' =>'扩展','icon' =>'x-diamond','anchor' =>'plugin']];
            if($min_plugin){
                $siderbar = empty($siderbar)?array_merge($plugin_home,$plugin_sider,$plugin_base):array_merge($siderbar,$plugin_sider,$plugin_base);
            }else{
                $siderbar = empty($siderbar)?array_merge($plugin_home,$plugin_sider):array_merge($siderbar,$plugin_sider);
            }
            return enjson(204,$siderbar);
        } catch (\Exception $e){
            return enjson(204);
        }
    }
    
    /**
     * 会员首页
     */
    public function login()
    {
        if (IS_POST) {
            $param = [
                'phone_id'       => $this->request->param('phone_id/d'),
                'login_password' => $this->request->param('password/s'),
                'captcha'        => $this->request->param('captcha/s'),
                'settemp'        => $this->request->param('settemp/d',0)
            ];
            $this->validate($param, 'Account.login');
            $rel = SystemTenant::where(['phone_id' => $param['phone_id']])->find();
            if (!$rel || !password_verify(md5($param['login_password']),$rel->password)) {
                return enjson(403,'登录失败或密码错误');
            }
            if($rel->is_lock){
                return enjson(403,'帐号已被锁定,请联系管理员解锁');
            }
            $rel->login_time = time();
            $rel->login_ip   = request()->ip();
            $rel->save();
            if ($rel) {
                $this->app->tenant->clearLogin();
                $apps_ids = SystemApps::where(['tenant_id' => $rel->id,'is_lock' => 0])->whereTime('end_time','<=',time())->column('id');
                if(!empty($apps_ids)){
                    SystemApps::where(['id' => $apps_ids])->update(['is_lock' => 1]);
                }
                if($rel->parent_id){
                    $result = SystemTenantApps::where(['tenant_id' => $rel->id,'is_lock' => 0])->field('apps_id')->order('is_default desc,id desc')->find();
                    if($result && !empty($result->apps) && !$result->apps->is_lock){
                        $this->app->tenant->setApps($result->apps_id);
                    }else{
                        $weapp =  SystemApps::where(['tenant_id' => $rel->id,'is_lock' => 0])->field('id')->order('is_default desc,id desc')->find();
                        if($weapp){
                            $this->app->tenant->setApps($weapp->id);
                        }
                    }
                }else{
                    $weapp =  SystemApps::where(['tenant_id' => $rel->id,'is_lock' => 0])->field('id')->order('is_default desc,id desc')->find();
                    if($weapp){
                        $this->app->tenant->setApps($weapp->id);
                    }else{
                        $result = SystemTenantApps::where(['tenant_id' => $rel->id])->field('apps_id')->order('is_default desc,id desc')->find();
                        if($result && !empty($result->apps) && !$result->apps->is_lock){
                            $this->app->tenant->setApps($result->apps_id);
                        }
                    }
                }
                $this->app->tenant->setLogin($rel->id);
                if($param['settemp'] == 1){
                    $this->app->tenant->setTempPassword(['user' => $param['phone_id'],'password' => $param['login_password']]);
                }else{
                    $this->app->tenant->clearTempPassword();
                }
                return enjson(302,['url' => (string)url('index/index')]);
            }
        } else {
            $view['qrcode'] = Config::get('config.we_account.account');
            $view['login']  = $this->app->tenant->getTempPassword();
            return view()->assign($view);
        }
    }

    /**
     * 生成登录二维码
     * @return void
     */
    public  function wechatQrcode(){
        if(IS_POST){
            $access_code = app('code')->en(uuid(0),'','ENCODE',60);
            $response = app('wechat')->client('admin')->data([
                'expire_seconds' => 60,
                'action_name' => 'QR_STR_SCENE',
                'action_info' => [
                    'scene' => [
                        'scene_str' => $access_code
                    ]
                ]
            ])->postJson('cgi-bin/qrcode/create');
            Cookie::set('login_ticket',$access_code,$response['expire_seconds']);
            return enjson(204,['url' => $response['url']]);
        }
        return enjson(404);
    }

    /**
     *检查绑定是否成功
     */
    public  function checkWechatQrcode(){
        if(IS_POST){
            if(Cookie::has('login_ticket')){
                $ticket = Cookie::get('login_ticket');
                $access_code = app('code')->de($ticket);
                if(!$access_code){
                    return enjson(403,'二维码已过期');
                }
                $tenant = SystemTenant::where(['ticket' => $access_code,'is_lock' => 0])->hidden(['safe_password,password,phone_id'])->find();
                if ($tenant) {
                    $tenant->login_time = time();
                    $tenant->login_ip   = request()->ip();
                    $tenant->ticket     = null;
                    $tenant->save();
                    $this->app->tenant->clearLogin();
                    //批量处理锁定应用
                    $apps_ids = SystemApps::where(['tenant_id' => $tenant->id,'is_lock' => 0])->whereTime('end_time','<=',time())->column('id');
                    if(!empty($apps_ids)){
                        SystemApps::where(['id' => $apps_ids])->update(['is_lock' => 1]);
                    }
                    //读取默认应用
                    $weapp = SystemApps::where(['tenant_id' => $tenant->id,'is_lock' => 0])->field('id')->order('is_default desc,id desc')->limit(2)->select();
                    if(!$weapp->isEmpty()){
                        $this->app->tenant->setApps($weapp->toArray()[0]['id']);
                    }
                    $this->app->tenant->setLogin($tenant->id);
                    return enjson(302,['url' => (string)url('index/index')]);
                }
            }
        }
        return enjson(204,'检测中'); 
    }

    /**
     * 会员注册
     */
    public function register()
    {
        if (request()->isPost()) {
            $param = [
                'phone_id'   => $this->request->param('phone_id/s'),
                'password'   => $this->request->param('password/s'),
                'repassword' => $this->request->param('repassword/s'),
                'sms_code'   => $this->request->param('sms_code/s'),
            ];
            $this->validate($param, 'Account.register');
            if (!app('sms')->isCode($param['phone_id'],$param['sms_code'])) {
                return enjson(403, '验证码错误');
            }
            //判断手机号是否重复
            $info = SystemTenant::where(['phone_id' => $param['phone_id']])->count();
            if ($info) {
                return enjson(403, '手机号重复');
            }
            //验证码通过
            $data['username']   = '用户_'.getcode(6);
            $data['phone_id']   = $param['phone_id'];
            $data['login_time'] = time();
            $data['login_ip']   = request()->ip();
            $data['parent_id']  = 0;
            if(empty($data['password'])){
                $data['password'] = password_hash(md5($param['password']), PASSWORD_DEFAULT);
            }
            if(empty($data['safe_password'])){
                $data['safe_password'] = password_hash(md5(substr($param['phone_id'],-6)), PASSWORD_DEFAULT);
            }
            $rel = SystemTenant::create($data);
            if ($rel) {
                $this->app->tenant->clearLogin();
                $this->app->tenant->setLogin($rel->id);
                return enjson(302,['url' => (string)url('index/index')]);
            }
        } else {
            return view();
        }
    }

    /**
     * 忘记密码
     */
    public function forgot()
    {
        if (request()->isPost()) {
            $param = [
                'phone_id'   => $this->request->param('phone_id/s'),
                'password'   => $this->request->param('password/s'),
                'repassword' => $this->request->param('repassword/s'),
                'sms_code'   => $this->request->param('sms_code/s'),
            ];
            $this->validate($param, 'Account.register');
            if (!app('sms')->isCode($param['phone_id'],$param['sms_code'])) {
                return enjson(403, '验证码错误');
            }
            $rel = SystemTenant::where(['phone_id' => $param['phone_id']])->find();
            if (!$rel) {
                return enjson(403, '找回密码失败');
            }
            $rel->login_time = time();
            $rel->login_ip   = request()->ip();
            $rel->password   = password_hash(md5($param['password']), PASSWORD_DEFAULT);
            $rel->save();
            if ($rel) {
                $this->app->tenant->clearLogin();
                $this->app->tenant->setLogin($rel->id);
                return enjson(302,['url' => (string)url('index/index')]);
            }
        } else {
            return view();
        }
    }

    /**
     * 会员退出
     */
    public function logout()
    {
        $this->app->tenant->clearLogin();
        return redirect((string)url('index/login'));
    }

    /**
     * 获取指定手机短信
     * @return void
     */
    public function getPhoneSms(){
        $phone_id = $this->request->param('phone_id/s');
        $this->validate(['phone_id' => $phone_id],'Account.getsms');
        $rel = app('sms')->getCode($phone_id);
        if(IS_DEBUG){
            return enjson(202,'Debug验证码:'.$rel['code']);
        }
        return enjson(204);
    }

    /**
     * 获取已绑定短信
     * @return void
     */
    public function getSms(){
        $phone_id = $this->request->param('phone_id/s');
        $this->validate(['phone_id' => $phone_id],'Account.getsms');
        $info = SystemTenant::where(['phone_id' => $phone_id])->find();
        if (!$info) {
            return enjson(403,'手机号未注册');
        }
        $rel = app('sms')->getCode($info->phone_id);
        if(IS_DEBUG){
            return enjson(202,'Debug验证码:'.$rel['code']);
        }
        return enjson(204);
    }
}