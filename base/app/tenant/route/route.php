<?php
/**
 * @copyright 2022 http://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 默认路由配置
 */
use think\facade\Route;
Route::rule('index', 'index/index');
Route::rule('login', 'index/login');
Route::rule('register','index/register');
Route::rule('forgot', 'index/forgot');
Route::rule('logout', 'index/logout');
Route::rule('account','account/index');
Route::rule('manage','account/manage');
Route::rule('upload','common/upload');
Route::rule('storage','common/storage');