<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 应用管理
 */

namespace base\app\admin\controller;

use base\model\SystemApp;
use base\model\SystemApps;
use base\model\SystemConfig;
use think\facade\Db;
use util\Sql;
use util\Dir;
use Exception;

class App extends Common
{
    protected $method = ['sort' => 'post'];

    /**
     * 应用列表
     * @access public
     */
    public function index(int $lock = 0)
    {
        $condition = [];
        if($lock <= 1) {
            $condition[] = ['is_lock','=',$lock ? 1 : 0];
        }
        $keyword = $this->request->param('keyword/s');
        if(!empty($keyword)) {
            $condition[] = ['title','like','%'.$keyword.'%'];
        }
        $view['list']   = SystemApp::where($condition)->withCount('apps')->order('sort desc,id desc')->paginate($this->pages);
        $view['offapp'] = SystemApp::offApp(); //未安装的APP
        $view['breadcrumb'] = [['name' =>'控制面板','icon' =>'window'],['name' =>'应用商店','url'=> (string)url('app/index')]];
        return view()->assign($view);
    }

    /**
     * 租户应用
     * @return void
     */
    public function details(int $types = 0)
    {
        $id = $this->request->param('id/d', 0);
        $info = SystemApp::where(['id' => $id])->find();
        if(empty($info)) {
            $this->error('系统中未找到安装的此应用', '友情提示');
        }
        $view['apps'] = SystemApps::where(['is_lock'=>$types,'app_id' => $id])->order('id desc')->select();
        $view['info'] = $info;
        $view['breadcrumb'] = [['name' =>'控制面板','icon' =>'window'],['name' =>'应用商店','url'=> (string)url('app/index')],['name' =>'租户应用']];
        return view()->assign($view);
    }

    /**
     * 编辑用户
     */
    public function edit()
    {
        if(IS_AJAX) {
            $data = [
                'id'         => $this->request->param('id/d', 0),
                'is_cloud'   => $this->request->param('is_cloud/d', 0),
                'cloud_url'  => $this->request->param('cloud_url/s'),
                'cloud_id'   => $this->request->param('cloud_id/s'),
                'cloud_key'  => $this->request->param('cloud_key/s'),
                'appname'    => $this->request->param('appname/s'),
                'title'      => $this->request->param('title/s'),
                'logo'       => $this->request->param('logo', ''),
                'about'      => $this->request->param('about'),
                'expire_day' => $this->request->param('expire_day/d'),
                'price'      => $this->request->param('price/f', 0),
                'qrcode'     => $this->request->param('qrcode/s'),
                'theme'      => $this->request->param('theme/a', []),
            ];
            $this->validate($data, $data['id'] ? 'App.edit' : 'App.install');
            if(isset(array_flip(FORBIDEN)[$data['appname']])) {
                return enjson(403, $data['appname'].'为禁止创建目录名称');
            }
            if($data['id']) {
                $is_update_app = SystemApp::where(['id' => $data['id']])->find();
                if(!$is_update_app) {
                    return enjson(403, '未找到应用');
                }
                if($is_update_app->is_cloud && empty($data['cloud_url'])) {
                    return enjson(403, '通信地址URL必须填写');
                }
                $data['is_cloud'] = $is_update_app->is_cloud;
            } else {
                if($data['is_cloud']) {
                    if(empty($data['cloud_url'])) {
                        return enjson(403, '通信地址URL必须填写');
                    }
                    $license = SystemConfig::configs('license');
                    if(empty($license['key']) || empty($license['cloudkey'])) {
                        return enjson(403, '应用密钥或主机秘钥未设置');
                    }
                    $data['appname'] = 'cloud';
                } else {
                    $is_create_app = SystemApp::where(['appname'=>strtolower($data['appname'])])->find();
                    if($is_create_app) {
                        return enjson(403, '禁止安装相同的应用目录');
                    }
                }
            }
            SystemApp::edit($data);
            return enjson(200, ['url' => (string)url('app/index')]);
        } else {
            $view['path'] = 'app';
            $info = SystemApp::where(['id' => $this->request->param('id/d')])->find();
            if($info && isset(array_flip(SYSAPP)[$info->appname])) {
                $view['path'] = 'base/app';
            }
            $view['info']    = $info;
            $view['license'] = SystemConfig::configs('license');
            $view['breadcrumb'] = [['name' =>'控制面板','icon' =>'window'],['name' =>'应用商店','url' => (string)url('app/index')],['name' =>'应用编辑']];
            return view()->assign($view);
        }
    }

    /**
     * 锁定
     * @param integer $id 用户ID
     */
    public function islock(int $id)
    {
        $result = SystemApp::where(['id' => $id])->find();
        if(!$result) {
            return enjson(403, '未找到应用');
        }
        $data['id']      = $result->id;
        $data['is_lock'] = $result->is_lock ? 0 : 1;
        $rel = SystemApp::update($data);
        return enjson($rel ? 204 : 403);
    }

    /**
     * 卸载应用
     * @access public
     * @return bool
     */
    public function delete(int $id)
    {
        $tenant = SystemApps::where(['app_id' => $id])->count();
        if($tenant) {
            return enjson(0, '当前应用已产生数据,禁止删除,请禁用');
        }
        $result = SystemApp::where(['id' => $id])->find();
        if(!$result) {
            return enjson(0, '未找到应用');
        }
        //云应用卸载(如果云应用超过1个以上,仅删除数据)
        if($result->is_cloud) {
            $cloud = SystemApp::where(['is_cloud' => 1])->count();
            if($cloud > 1) {
                SystemApp::destroy($id);
                return enjson(200);
            }
        }
        //卸载应用的SQL文件
        $app_path = isset(array_flip(SYSAPP)[$result->appname]) ? PATH_SYSAPP : PATH_APP; //应用路径
        $file = $app_path.$result->appname.DS.'package'.DS.'database'.DS.'uninstall.sql';
        if (file_exists($file)) {
            $array = Sql::sqlAarray($file, DB_PREFIX);
            foreach ($array as $sql) {
                Db::query($sql);
            }
        }
        //删除静态资源
        Dir::rmDirs(PATH_STATIC.DS.$result->appname.DS);
        //删除安装数据
        SystemApp::destroy($id);
        return enjson(200);
    }

    /**
     * 排序
     */
    public function sort()
    {
        $data = [
            'sort' => $this->request->param('sort/d'),
            'id'   => $this->request->param('id/d'),
        ];
        $this->validate($data, 'App.sort');
        SystemApp::update(['sort'=>$data['sort'],'id' => $data['id']]);
        return enjson(302);
    }

    /**
     * 切换应用管理
     * */
    public function manage(int $id = 0)
    {
        $info = SystemApp::where(['id' => $id])->find();
        if(!$info) {
            return enjson(0, '未找到管理的应用');
        }
        $config = $this->app->configs->version($info->appname);
        if(empty($config)) {
            return [];
        }
        if(!$config['is_admin']) {
            return enjson(0, '当前应用没有独立管理中心');
        }
        $this->app->admin->clearApp();        //清除应用
        $this->app->admin->setApp(['appid' => $info->id,'appname' => $info->appname]); //切换应用
        return enjson(204);
    }

    /**
     * @param $appname
     * @return \think\response\Json
     * 安装程序
     */
    public function install()
    {
        try {
            $appname  = $this->request->param('appname');
            if(empty($appname)) {
                return enjson(403, '安装应用未找到');
            }
            if(isset(array_flip(FORBIDEN)[$appname])) {
                return enjson(403, $appname.'为禁止创建目录名称');
            }
            $is_create_app = SystemApp::where(['appname' => strtolower($appname)])->find();
            if($is_create_app) {
                return enjson(403, '禁止安装相同的应用目录');
            }
            $app_path = (isset(array_flip(SYSAPP)[$appname]) ? PATH_SYSAPP : PATH_APP).$appname.DS; //应用路径
            if(!is_dir($app_path)) {
                return enjson(403, '未找到安装应用路径');
            }
            $param = $this->app->configs->version($appname);
            if(empty($param)) {
                return enjson(403, '未找到应用配置信息');
            }
            //检测目录权限
            if (!Dir::isDirs(PATH_STORAGE)) {
                return enjson(0, '私有资源 private 目录权限不足');
            }
            //静态资源路径
            if (!Dir::isDirs(PATH_STATIC)) {
                return enjson(0, '静态资源 static 目录权限不足');
            }
            //路径与锁文件
            $static_dir   = PATH_STATIC.$appname.DS;
            $private_dir  = PATH_STORAGE.DS.'install'.DS;
            $install_lock = 'app_'.$appname.'.lock';
            if (file_exists($private_dir.$install_lock)) {
                return enjson(0, '请删除[private/install/'.$install_lock.']文件锁');
            }
            $app = SystemApp::column('appname');
            if(in_array($appname, $app)) {
                return enjson(403, '应用已安装,禁止重复安装');
            }
            //插入数据
            $data = [
                'appname'    => $appname,
                'title'      => $param['name'],
                'logo'       => DOMAIN.'static/'.$appname.'/logo.png',
                'about'      => $param['about']??'',
                'expire_day' => 7,
                'price'      => 0,
                'is_cloud'   => $appname == 'cloud' ? 1 : 0
            ];
            $this->validate($data, 'App.install');
            $install_sql = $app_path.'package'.DS.'database'.DS.'install.sql';
            if (file_exists($install_sql)) {
                //创建静态资源
                if(!Dir::mkdirs($static_dir)) {
                    return enjson(403, '静态资源目录创建失败');
                }
                //复制资源到静态目录
                $static = $app_path.'package'.DS.'static';
                if (file_exists($static)) {
                    Dir::copyDirs($static, $static_dir);
                }
                //创建应用锁
                @touch($private_dir.$install_lock);
                //执行安装
                $array = Sql::sqlAarray($install_sql, DB_PREFIX);
                foreach ($array as $sql) {
                    Db::query($sql);
                }
                SystemApp::edit($data);
                return enjson(200, '应用安装成功');
            } else {
                return enjson(403, '未找到数据库脚本');
            }
        } catch (Exception $e) {
            return enjson(403, $e->getMessage());
        }
    }
}
