<?php
/**
 * @copyright 2022 http://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 默认路由配置
 */

use think\facade\Route;
//控制台
Route::rule('index', 'index/index');
Route::rule('login', 'index/login');
Route::rule('password','index/password');
Route::rule('logout', 'index/logout');
Route::rule('totanent_<id>', 'apps/toTanent');
Route::rule('upload','common/upload');