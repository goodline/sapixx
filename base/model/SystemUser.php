<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 用户表
 */

namespace base\model;

use base\BaseModel;
use invite\Invite;
use think\Request;

class SystemUser extends BaseModel
{
    /**
      * 关联多端帐号
      */
    public function client()
    {
        return $this->hasOne('SystemUserRuid', 'uid');
    }

    /**
     * 新增更新服务ID邀请码
     * @param object $data
     * @return void
     */
    public static function onAfterInsert($data)
    {
        //更新邀请码
        $data->invite_code = Invite::enCode($data->id);
        $data->save();
        //创建后事件
        try {
            invoke(fn (Request $request) => eventer('UserAfter', empty($request->app->appname) ? 'tenant' : $request->app->appname, $data));
        } catch (\Exception $e) {}
    }

    /**
     * 搜索
     * @param object $query
     * @param string $value
     * @return void
     */
    public function searchKeywordAttr($query, $value)
    {
        if(!empty($value)) {
            if(validate()->isMobile($value)) {
                $query->where('phone', '=', $value);
            } else {
                $query->where('nickname', 'like', '%'.$value.'%');
            }
        }
    }

    //用户头像
    public function getFaceAttr($value)
    {
        return $value ?: DOMAIN.'common/img/nickname.png';
    }

    //用户昵称
    public function getNicknameAttr($value)
    {
        return $value ?: '匿名用户';
    }

    //绑定手机号别名
    public function getAsPhoneAttr($value, $data)
    {
        return $data['phone'] ?: '未绑定';
    }

    /**
     * 查找邀请用户
     * @param string  $invite_code  邀请码
     */
    public static function isInvite($invite_code)
    {
        if(empty($invite_code)) {
            return;
        }
        $uid = Invite::deCode($invite_code);
        if(empty($uid)) {
            return;
        }
        $invite = self::apps()->where(['id' => $uid])->field('id')->find();
        return $invite->id ?? false;
    }

    /**
     * 创建微信用户
     * @param string  $invite_code 邀请码
     */
    public static function addWechatUser($data)
    {
        try {
            if(empty($data['unionid'])) {
                return self::getCreateUser($data);
            } else {
                $rel = self::apps()->where(['unionid' => $data['unionid'],'is_delete' => 0])->find();
                if($rel) {
                    if($rel->is_lock) {
                        return;
                    }
                    if(isset($data['nickname']) && isset($data['face'])) {
                        $rel->nickname = $data['nickname'];
                        $rel->face     = $data['face'];
                        $rel->save();
                    }
                    return $rel;
                } else {
                    return self::getCreateUser($data,$data['unionid']);
                }
            }
        } catch (\Exception $e) {
            return;  
        }
    }  

    /**
     * 获取登录用户并更新
     * @param array $data
     * @param string $unionid
     * @return void
     */
    public static function getCreateUser($data,$unionid = null){
        $ruid = SystemUserRuid::apps()->where(['client_id' => $data['client_id'],'login_id' => $data['openid'],'is_delete' => 0])->find();
        if(empty($ruid)) {
            return self::createUser($data);
        }
        $rel = self::find($ruid->uid);
        if($rel->is_lock) {
            return;
        }
        if($unionid){
            $rel->unionid = $unionid;
        }
        if(isset($data['nickname']) && isset($data['face'])) {
            $rel->nickname = $data['nickname'];
            $rel->face     = $data['face'];
        }
        $rel->save();
        return $rel;
    }

    /**
     * 创建用户
     */
    public static function createUser($data)
    {
        $result = self::create($data);
        if(empty($result)){
            return;
        }
        //创建关系
        if(!empty($data['invite'])) {
            $invite = self::isInvite($data['invite']);
            if($invite) {
                SystemUserRelation::addLayer($result->id,$invite);
            }
        }
        return self::find($result->id);
    }
}