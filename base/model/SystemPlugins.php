<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 租户应用
 */

namespace base\model;

use base\BaseModel;
use think\Request;

class SystemPlugins extends BaseModel
{
    protected $autoWriteTimestamp = true;

    /**
     * 关联应用
     */
    public function apps()
    {
        return $this->hasOne('SystemApps', 'id', 'apps_id')->cache(300);
    }

    /**
     * 应用后台所属管理员
     */
    public function tenant()
    {
        return $this->hasOne('SystemTenant', 'id', 'tenant_id')->cache(300);
    }

    /**
     * 所属插件
     */
    public function plugin()
    {
        return $this->hasOne('SystemPlugin', 'id', 'plugin_id')->cache(300);
    }

    /**
     * 获取权限组ID
     * @return array
     */
    public function getGroupIdsAttr($value)
    {
        return empty($value) ? [] : explode(',', $value);
    }

    //我的扩展状态
    public function getLockTextAttr($value, $data)
    {
        $status = [0 => '正常',1 => '锁定'];
        return $status[$data['is_lock']];
    }

    /**
    * 判断是否开通了插件和你是否有权限
    * @return array
    */
    public static function is_plugin($plugin, $only_reg = false)
    {
        return invoke(function (Request $request) use ($plugin, $only_reg) {
            $plugin = SystemPlugin::where(['appname' => $plugin])->cache()->field('id')->find();
            if(!$plugin) {
                return false;
            }
            $plugins = SystemPlugins::where(['apps_id' => $request->apps->id,'plugin_id' => $plugin->id])->cache(120)->field('id,is_lock,group_ids')->find();
            if(empty($plugins)) {
                return false;
            }
            if($only_reg) {
                return true;
            }
            if($plugins->is_lock || empty($request->tenant)) {
                return false;
            }
            if(!$request->tenant->is_manage) {
                if(empty($plugins->group_ids)) {
                    return false;
                }
                if(!in_array($request->tenant->group_id, $plugins->group_ids)) {
                    return false;
                }
            }
            return true;
        });
    }
}
