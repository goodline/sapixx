<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 *  会员消费账单
 */

namespace base\model;

use base\BaseModel;

class SystemTenantBill extends BaseModel
{
    /**
     * 根据订单状态获取状态文本
     * @param string $value 原始状态文本值（未使用）
     * @param array $data 订单数据数组，包含状态键（state）
     * @return string 返回订单的状态文本，"成功"或"失败"
     */
    public function getStateTextAttr($value, $data)
    {
        return $data['state'] ? '成功' : '失败';
    }

    /**
     * 根据订单金额判断交易类型
     * @param mixed $value 本参数在当前上下文中未直接使用，保留以符合函数签名要求。
     * @param array $data 包含订单相关数据的数组，其中应包含'money'键用于判断交易类型。
     * @return string 返回交易类型的字符串表示，可能是"充值"或"消费"。
     */
    public function getTypesAttr($value, $data)
    {
        return $data['money'] ? '充值' : '消费';
    }
}
