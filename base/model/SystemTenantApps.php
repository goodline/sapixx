<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 租户子应用管理
 */

namespace base\model;

use base\model\SystemApps;
use base\BaseModel;

class SystemTenantApps extends BaseModel
{
    protected $autoWriteTimestamp = true;

    /**
      * 关联的租户
      */
    public function tenant()
    {
        return $this->hasOne('SystemTenant', 'id', 'tenant_id');
    }

    /**
     * 权限组
     */
    public function group()
    {
        return $this->hasOne(SystemTenantGroup::class, 'id', 'group_id');
    }

    /**
     * 管理应用
     */
    public function apps()
    {
        return $this->hasOne(SystemApps::class, 'id', 'apps_id');
    }
}
