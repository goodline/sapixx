<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 租户中间件(权限判断)
 */

namespace base\middleware;

use think\App;
use think\facade\View;
use base\model\SystemTenantGroup;

class Acl
{
    /** @var App */
    protected $app;

    public function __construct(App $app)
    {
        $this->app  = $app;
    }

    public function handle($request, \Closure $next)
    {
        $controller = strtolower(trim($request->controller()));
        $action     = strtolower(trim($request->action()));
        /**
         * 1、确定是子帐号
         * 2、确定有权限组
         * 3、确定帐号是当前应用的子帐号
         */
        $request->acl  = false;
        $request->rank = 0;
        if(!$request->tenant->is_manage && $request->tenant->group_id) {
            $info = SystemTenantGroup::where(['id' => $request->tenant->group_id])->apps()->field('title,apps_id,rank,rules')->find();
            if(empty($info->rules)) {
                $this->errorInfo(403, '你没有任何权限,请联系管理增加');
            }
            $info->rules = $info ? array_flip($info->rules) : [];
            $acl_path = md5($controller.'.'.$action); //当前访问的路由值
            if(!isset($info->rules[$acl_path])) {
                $this->errorInfo(403, '你无权访问当前路径');
            }
            $request->acl  = $info;
            $request->rank = $info->rank;
        }
        View::assign(['acl' => $request->acl,'rank' => $request->rank]);
        return $next($request);
    }

    /**
     * 打印错误(兼容性代码)
     * @return void
     */
    public function errorInfo(int $code, $message)
    {
        return (IS_AJAX || IS_JSON) ? exitjson($code, $message) : abort($code, $message);
    }

    public function end(\think\Response $response)
    {
        // 回调行为
    }
}
