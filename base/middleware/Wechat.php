<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * 微信公众号中间件
 */

namespace base\middleware;

use think\App;
use think\facade\View;
use base\model\SystemAppsClient;

class Wechat
{
    /** @var App */
    protected $app;

    public function __construct(App $app)
    {
        $this->app  = $app;
    }

    public function handle($request, \Closure $next)
    {
        //读取站点信息
        $client = $request->client ?: self::getApps();
        if(empty($client)) {
            abort(404, '访问应用不存在或未开通服务');
        }

        //应用信息
        $request->client  = $client;
        $request->apps    = $client->apps;
        $request->app     = $client->app;
        $request->config  = (object)$this->app->config->get('config');
        $request->configs = (object)$this->app->config->get('version');
        $request->web     = $request->config->site;
        $request->user    = self::getUser();  //登录用户

        //赋值到模版使用
        View::assign([
            'web'     => $request->web,
            'user'    => $request->user,
            'apps'    => $request->apps,
            'app'     => $request->app,
            'configs' => (array)$request->configs
        ]);

        return $next($request);
    }

    /**
     * 读取登录用户
     * @return bool|object
     */
    protected function getUser()
    {
        //记录邀请码
        $invite = $this->app->request->param('invite/s',null);
        if($invite){
            session('invite',$invite);
        }
        //获取登录用户信息
        return $this->app->user->getLogin($this->app->request->param('token/s',''));
    }

    /**
     * 读取应用信息
     * @return bool|object
     */
    protected function getApps()
    {
        $client_id = $this->app->request->param('get/s');
        if (empty($client_id) || !preg_match('/^([a-zA-Z0-9]){6}$/', $client_id)) {
            return false;
        }
        return SystemAppsClient::where(['id' => idcode($client_id)])->cache(true)->find();
    }

    public function end(\think\Response $response)
    {
        // 回调行为
    }
}
