<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * API用户登录验证中间件
 */

namespace base\middleware;

use think\App;

class ApiAcl
{
    /** @var App */
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function handle($request, \Closure $next)
    {
        if(!$request->user) {
            if(IS_AJAX || IS_JSON){
                return enjson(401, '帐号未登录,禁止访问');
            }else{
                $this->loginUri($request->client->title);
            }
        }
        return $next($request);
    }

    public function end(\think\Response $response)
    {
        // 回调行为
    }

    /**
     * 读取应用信息
     * @return bool|object
     */
    protected function loginUri($client)
    {

        $data['get']      = $this->app->request->client->idcode;
        $data['invite']   = session('?invite') ? session('invite'): '';
        $data['callback'] = $this->app->request->param('callback/s')?:urlencode($this->app->request->domain().$this->app->request->url());
        switch ($client) {
            case 'wechatmp': //公众号授权
                $url = app('wechat')->new()->getOauth()->scopes(['snsapi_userinfo'])->redirect(apis('service/login/official',$data));
                break;
            default: //预留授权(如企业微信等)
                $url = $data['callback'];
                break;
        }
        return redirect($url);
    }
}
