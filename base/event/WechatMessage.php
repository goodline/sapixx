<?php
/**
 * @copyright   Copyright (c) 2017-2030  https://www.sapixx.com All rights reserved.
 * @license Licensed (https://www.gnu.org/licenses/gpl-3.0.txt).
 * @link https://www.sapixx.com
 * 独立应用消息事件
 */
declare (strict_types=1);

namespace base\event;

use base\model\SystemAppsRelease;

class WechatMessage
{
    /**
      * 小程序审核状态推送
      * @param array $param ['message' => '服务消息,'client' ='客户端对象']
      */
    public function onWeapp($param)
    {
        if(!empty($param['client'])) {
            $client  = $param['client'];
            $release = SystemAppsRelease::where(['apps_id' => $client->apps_id,'client_id' => $client->id])->find();
            if(empty($release)) {
                return 'fail';
            }
            if($release->is_commit == 2) {
                $message = $param['message'];
                switch ($message['Event']) {
                    case 'weapp_audit_success':
                        $release->state     = 0;
                        $release->is_commit = 2;
                        break;
                    case 'weapp_audit_fail':
                        $release->is_commit = 0;
                        $release->state     = 0;
                        $release->reason    = $message['Reason'];
                        break;
                    case 'weapp_audit_delay':
                        $release->state     = 1;
                        $release->reason    = $message['Reason'];
                        break;
                }
                $release->save();
                return 'success';
            }
        }
        return 'fail';
    }

    /**
     * 默认消息处理
     * @param array $param ['message' => '服务消息,'client' ='客户端对象']
     */
    public function onMessage($param)
    {
        return '信息';
    }

    /**
     *扫码
     * @param array $param ['message' => '服务消息,'client' ='客户端对象']
     */
    public function onScan($param)
    {
        return '扫码';
    }

    /**
     *关注
     * @param array $param ['message' => '服务消息,'client' ='客户端对象']
     */
    public function onSubscribe($param)
    {
        return '关注';
    }

    /**
     *取关
     * @param array $param ['message' => '服务消息,'client' ='客户端对象']
     */
    public function onUnsubscriben($param)
    {
        return '取关';
    }
}
