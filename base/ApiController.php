<?php
/**
 * @copyright 2022 https://www.sapixx.com All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt
 * @link https://www.sapixx.com
 * @author pillar<ltmn@qq.com>
 * API接口访问基础控制器
 */

namespace base;

class ApiController extends BaseController
{
    //定义默认API中间件
    protected $middleware = ['base\middleware\Api'];
    //强制登录验证
    protected $acl         = [];
    //免于登录控制
    protected $aclOff      = [];

    /**
     * 初始化
     */
    protected function initialize()
    {
        if(!empty($this->acl) || !empty($this->aclOff)) {
            if(empty($this->acl)) {
                $this->middleware['base\middleware\ApiAcl::class'] = ['except' => $this->aclOff];
            } else {
                $this->middleware['base\middleware\ApiAcl::class'] = ['only' => $this->acl];
            }
        }
    }

    /**
     * SAPI++引擎版本检查
     */
    public function checkVar()
    {
        return enjson(200, SYSNAME.BASEVER);
    }
}
