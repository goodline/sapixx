<?php
/**
 * siderbar侧边栏
 * anchor 锚点对应中config目录下菜单组文件名
 * icon:https://icons.bootcss.com/
 */
return [
    [
        'name'   => '首页',
        'icon'   => 'house',
        'anchor' => 'tenant'
    ],
    [
        'name'   => '商城',
        'icon'   => 'shop',
        'anchor' => 'shop'
    ],
];