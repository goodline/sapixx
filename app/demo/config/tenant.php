<?php
/**
 * 管理端管理菜单
 * icon:https://icons.bootcss.com/
 */
return [
    [
        'name' => '实例首页',
        'icon' => 'code-square',
        'menu' => [
            ['name' => '欢迎首页', 'url' => (string)url('demo/tenant/index')]
        ]
    ]
];
